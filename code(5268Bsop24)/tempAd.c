
#define __tempAd_c
#include    "includeAll.h"
//#define D_pullUp10K    1
//#define D_pullDown10K   1
//10k  12bits Ad sample
//0c--99c
#define D_tableSize 100
//pullup 10k
code const uint16_t adTable[D_tableSize] = {
	3974, 3968, 3962, 3955, 3949, 3942, 3935, 3927, 3919, 3911, 3901, 3892, 3882,
	3873, 3863, 3853, 3842, 3831, 3820, 3808, 3795, 3782, 3768, 3753, 3738, 3723,
	3707, 3692, 3677, 3663, 3650, 3633, 3616, 3599, 3580, 3560, 3539, 3517, 3495,
	3471, 3448, 3424, 3400, 3376, 3353, 3332, 3306, 3281, 3255, 3229, 3202, 3176,
	3149, 3122, 3094, 3066, 3037, 3007, 2977, 2945, 2912, 2881, 2849, 2817, 2784,
	2751, 2718, 2684, 2650, 2616, 2582, 2548, 2514, 2480, 2447, 2413, 2381, 2349,
	2317, 2285, 2253, 2221, 2189, 2157, 2124, 2091, 2058, 2024, 1990, 1955, 1919,
	1885, 1853, 1821, 1790, 1759, 1728, 1698, 1668, 1638,
};
//pulldown 10k
// code const uint16_t adTable[D_tableSize] = {
//  122, 128, 134, 141, 147, 154, 161, 169, 177, 185, 194, 203, 213, 222, 232, 242,
//  253, 264, 275, 287, 300, 314, 328, 342, 357, 372, 388, 403, 418, 432, 446, 462,
//  479, 497, 515, 535, 556, 578, 600, 624, 647, 672, 695, 719, 742, 763, 789, 815,
//  840, 866, 893, 919, 946, 973, 1001, 1029, 1058, 1088, 1119, 1150, 1183, 1214,
//  1246, 1278, 1311, 1344, 1378, 1411, 1445, 1479, 1514, 1548, 1582, 1615, 1649,
//  1682, 1714, 1746, 1778, 1810, 1842, 1874, 1906, 1938, 1971, 2004, 2037, 2071,
//  2106, 2141, 2177, 2210, 2243, 2275, 2306, 2337, 2367, 2397, 2427, 2457,
// };
// return 0: short or off
// return 1--100c
void GetTemp(void)
{
//var
	static uint16_t adFilterResult = 3650;
	static uint16_t adFilterResult2 = 3650;
	// static uint16_t adFilterResult = 672;
	uint16_t adCurResult;
	uint8_t head, middle, end;
//code
	//ad4
	P1MODL |= 0x03;  //P1.0
	CHSEL = D_adTempChannel + 0x0f;    //AD4
	OPTION = 0x08;   //ADC Clock Rate: SYSCLK/8
	CLRWDT = 1;
	ADSOC = 1;
	_nop_();
	_nop_();
	while (ADSOC) {
	}
	adCurResult = ADCDH;
	adCurResult <<= 4;
	head = ADTKDT;
	head >>= 4;
	adCurResult += head;
	if (adCurResult >= D_offValue) {
		bKettleValid = 0;
		padInvalidTimer = D_padInvalidTime;
		return;
	}else {
		bKettleValid = 1;
		adFilterResult2 = (adFilterResult2 * 7 + adCurResult) / 8;
		adFilterResult = (adFilterResult * 7 + adFilterResult2) / 8;
		head = 0;
		end = D_tableSize - 1;
		middle = (head + end) / 2;
		while (head != end) {
			if (adFilterResult < adTable[middle]) {
				head = middle + 1;
				middle = (head + end) / 2;
			}else {
				if (adFilterResult > adTable[middle]) {
					end = middle;
					middle = (head + end) / 2;
				}else {
					//==
					break;
				} //end if
			}// end if
		}//end while
		curTemp = middle + 6;
		if (curTemp > 100) {
			curTemp = 100;
		}
	}
}
