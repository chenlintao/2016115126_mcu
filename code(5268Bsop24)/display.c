
#define __dispaly_c
#include    "includeAll.h"
//共阴正向
code const uint8_t displayFonts[10] = { 0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F };
//------------------
void DisplayScan(void)
{
	static uint8_t dspIndex = 0x0;

	F_segsOff();
	F_comsOff();
	dspIndex++;
	if (dspIndex >= D_DisplayBufferSize) {
		dspIndex = 0;
	}
	if (displayBuffer[dspIndex] & 0x01) {
		//A
		F_setA();
	}
	if (displayBuffer[dspIndex] & 0x02) {
		//B
		F_setB();
	}
	if (displayBuffer[dspIndex] & 0x04) {
		//C
		F_setC();
	}
	if (displayBuffer[dspIndex] & 0x08) {
		//D
		F_setD();
	}
	if (displayBuffer[dspIndex] & 0x10) {
		//E
		F_setE();
	}
	if (displayBuffer[dspIndex] & 0x20) {
		//F
		F_setF();
	}
	if (displayBuffer[dspIndex] & 0x40) {
		//G
		F_setG();
	}
	switch (dspIndex) {
		case 0:
			F_ledComOn();
			break;
		case 1:
			F_com1On();
			break;
		case 2:
			F_com2On();
			break;
		case 3:
			F_com3On();
			break;
		case 4:
			F_com4On();
			break;
	}//end switch
}
//------------------
void DisplayNumber(uint8_t number)
{
	uint8_t i;

	i = number / 100;
	if (i == 0) {
		displayBuffer[3] = D_fontBlank;
	}else {
		displayBuffer[3] = displayFonts[i];
	}
	number = number % 100;
	i = number / 10;
	displayBuffer[2] = displayFonts[i];
	number = number % 10;
	displayBuffer[1] = displayFonts[number];
	return;
}
//----------------------
void DisplayYyTime(uint16_t time)
{
	uint8_t i;

	if (time < 60) {
		i = time / 10;
		displayBuffer[3] = D_fontBlank;
		displayBuffer[2] = displayFonts[i];
		i = time % 10;
		displayBuffer[1] = displayFonts[i];
		F_jsIconOn();
	}else {
		i = time % 60;
		if (i == 0) {
			time = time / 60;
		}else{
			time = time / 60 + 1;
		}
		i = time / 10;
		if (i == 0) {
			displayBuffer[3] = D_fontBlank;
		}else {
			displayBuffer[3] = displayFonts[i];
		}
		i = time % 10;
		displayBuffer[2] = displayFonts[i];
		displayBuffer[1] = D_fontH;
		F_jsIconOff();
	}
	return;
}
