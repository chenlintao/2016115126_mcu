#ifndef  __tempAd_h
    #define __tempAd_h
//Hal
	#define P_temp	P1_0
	//ad4
    #define D_adTempChannel  0x40
//Const
    #define D_shortValue 400
    #define D_offValue   4000
	#define D_padInvalidTime 3  //s
    #define D_tempChangeTime 30 //s
//Var
	#ifdef __tempAd_c
		bit bKettleShort ;
		bit bKettleOff ;
		bit bKettleValid ;
        bit bHighTemp ;
		bit bAdTest ;
		bit bWaterTest ;
		bit bLowWater ;
		uint8_t padInvalidTimer ;
		uint8_t curTemp , lastTemp, highTemp;
        uint8_t tempChangeTimer ;
	#else
		extern bit  bKettleShort ;
		extern bit  bKettleOff ;
		extern bit  bKettleValid ;
        extern bit  bHighTemp ;
		extern bit  bAdTest ;
		extern bit  bWaterTest ;
		extern bit  bLowWater ;
		extern uint8_t	padInvalidTimer ;
		extern uint8_t curTemp , lastTemp, highTemp ;
        extern uint8_t tempChangeTimer ;
	#endif
//Function
    void    GetTemp(void)  ;
    void	GetWaterLevel(void) ;
#endif
