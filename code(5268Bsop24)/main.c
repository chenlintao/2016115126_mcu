//=============================================================================
//ALL IO PINS' Status is save to a GLOBAL RAM address(P0Temp,P1Temp,P2Temp,P3Temp)
//ex: if u want change P0.1 status,please do not use "=" to write P0 directly
//u should use "and" or "or" to change the P0Temp,and then write P0Temp to P0
//=============================================================================
//=============================================================================
//Name		:main.c
//Purpose	:main loop
//=============================================================================
#define     MAIN_C

#include    "includeAll.h"
//=============================================================================
void main(void)
{
	ChangeToFastClk();
	InitialIoport();
	InitialRegister();
	InitVar();
	PowerOff();
	//global interrupt enable
	EA = 1;
	Buzz();
	F_turnOnWDT();
	while (1) {
		CLRWDT = 1;
		TasksProcess();
		UserSettingProcess();
		TimersProcess();
	}//end while
}
//----------------------
void InitVar()
{
	bPowerOn = 0;
	bAlarm = 0;
	bSelfTest = 1;
	bKettleValid = 0;
	bKettleOff = 0;
	bKettleShort = 0;
	bYs = 0;
	bYw = 0;
	bYsHw = 0;
	bYwHw = 0;
	bDisplayScan = 1;
	tempChangeTimer = D_tempChangeTime;
	highTemp = 100;
	timer1Click = 0;
	timer4ms = 0;
	timer250ms = D_250ms;
	timer1s = D_1s;
	timer1m = D_1m;
	zeroCounter = 0;
	InitTriac();
}
//---------------------------------------------------
void PowerOff()
{
	bPowerOn = 0;
	bIdle = 1;
	b100Buzz = 0;
	bSel = 0;
	bYw = 0;
	bYs = 0;
	bXz = 0;
	bYy = 0;
	bSs = 0;
	bYwHw = 0;
	bYsHw = 0;
	bHeat = 0;
	bMicroHeat = 0;
	bWater100 = 0;
	bWf = 0;
	bSettedTime = 0;
	bWattSetted = 0;
	fireType = D_0w;
	yyTimer = 0;
}
//-------------------------
void GetKey()
{
	static int16_t lastTkStatus = 0;
	uint8_t mask, i;
	   #ifdef POWERSAVE
	PRO_SLEEP20MS();
		#else
	if (gBTimer0RollOver >= 5)
		#endif
	{
		gBTimer0RollOver = 0;
		TKScan();
		mask = 0x01;
		for (i = 0; i < TKCOUNT; i++) {
			if (((lastTkStatus & mask) == 0) && (TkStatus & mask)) {
				keyValue = i + 1;
				break;
			}
			mask <<= 1;
		}
		lastTkStatus = TkStatus;
	}
}
//----------------------
void TasksProcess()
{
	//kettleValid Pro
	if (bKettleValid == 1) {
		noOperationTimer = D_noOperationTime;
		//no water Pro
		if (zeroCounter > 250) {
			bAlarm = 1;
			buzzCounter = 5;
			zeroCounter = 0;
			PowerOff();
		}
	}else {
		bWater100 = 0;
		bMicroHeat = 0;
		bHeat = 0;
		bWattSetted = 0;
		bAlarm = 0;
	}
	//highTemp Pro
	if (bHighTemp == 0) {
		if ((curTemp >= 80) && (bSs == 1)) {
			if (padInvalidTimer == 0) {
				if ((curTemp != lastTemp)) {
					lastTemp = curTemp;
					tempChangeTimer = D_tempChangeTime;
				}else {
					if (tempChangeTimer == 0) {
						highTemp = curTemp - 1;
						bHighTemp = 1;
					}
				}
			}else {
				tempChangeTimer = D_tempChangeTime;
			}
		}else {
			tempChangeTimer = D_tempChangeTime;
		}
	}else {
		tempChangeTimer = D_tempChangeTime;
	}
	//yy Pro
	if ((bYy == 1) && (yyTimer == 0)) {
		bYy = 0;
	}
	if (bYy == 0) {
		//ss Pro
		if ((padInvalidTimer == 0) && (bSs == 1)) {
			if ((curTemp >= highTemp) && (bWater100 == 0)) {
				bHighTemp = 1;
				bWater100 = 1;
				bHeat = 0;
				bMicroHeat = 0;
				if (bYw == 1) {
					bMicroHeat = 1;
					bWf = 1;
					if (b100Buzz) {
						wfTimer = 30;
					}else{
						wfTimer = D_wfTime;
					}
					bWattSetted = 0;
					fireType = D_320w;
					bWattSetted = 1;
					if (bSettedTime == 0) {
						bSettedTime = 1;
						timer1m = 0;
						workTimer = settedTime;
					}
				}
				if (bYs == 1) {
					bMicroHeat = 1;
					bWf = 1;
					if (b100Buzz) {
						wfTimer = 30;
					}else{
						wfTimer = D_wfTime;
					}
					bWattSetted = 0;
					fireType = D_320w;
					bWattSetted = 1;
					if (bSettedTime == 0) {
						bSettedTime = 1;
						timer1m = 0;
						workTimer = settedTime;
					}
				}
				if (b100Buzz == 0) {
					b100Buzz = 1;
					Buzz();
				}
			}
			//
			if ((curTemp >= (highTemp - D_temp)) && (curTemp < highTemp)) {
				bHeat = 0;
				if ((bMicroHeat == 0)) {
					bMicroHeat = 1;
					bWattSetted = 0;
					if (bYs == 1) {
						fireType = D_580w;
					}
					if (bYw == 1) {
						fireType = D_620w;
					}
					bWattSetted = 1;
				}
				//处理温度在沸点处抖动问题
				if (curTemp < (highTemp - 2)) {
					if (bWater100 == 1) {
						bWater100 = 0;
						bMicroHeat = 0;
					}
				}
			}
			if ((curTemp < (highTemp - D_temp))) {
				bMicroHeat = 0;
				if (bHeat == 0) {
					bHeat = 1;
					bWattSetted = 0;
					fireType = D_1000w;
					bWattSetted = 1;
				}
				bWater100 = 0;
				b100Buzz = 0;
			}
		}
		//wf Pro
		if ((bWf == 1) && (wfTimer == 0)) {
			bWf = 0;
			bMicroHeat = 1;
			bWattSetted = 0;
			if (bYw) {
				fireType = wattTable[ywWattTable[adjustIndex]];
			}
			if (bYs) {
				fireType = wattTable[ysWattTable[adjustIndex]];
			}
			bWattSetted = 1;
		}
		//workTimer Pro
		if ((bYw == 1) && (workTimer == 0)) {
			buzzCounter = 3;
			bYw = 0;
			bYwHw = 1;
			bYsHw = 0;
			bSs = 0;
			bMicroHeat = 0;
			workTimer = D_hwTime;
		}
		if ((bYs == 1) && (workTimer == 0)) {
			buzzCounter = 3;
			bYs = 0;
			bYsHw = 1;
			bYwHw = 0;
			bSs = 0;
			bMicroHeat = 0;
			workTimer = D_hwTime;
		}
		//hw pro
		if (padInvalidTimer == 0) {
			if (bYwHw == 1) {
				if (workTimer == 0) {
					Buzz();
					PowerOff();
				}else {
					if (curTemp < 60) {
						if (bMicroHeat == 0) {
							bMicroHeat = 1;
							bWattSetted = 0;
							fireType = D_160w;
							bWattSetted = 1;
						}
					}else {
						bWattSetted = 0;
						fireType = D_0w;
						bMicroHeat = 0;
					}
				}
			}
			if ((bYsHw == 1)) {
				if (workTimer == 0) {
					Buzz();
					PowerOff();
				}else {
					if (curTemp < 60) {
						if (bMicroHeat == 0) {
							bMicroHeat = 1;
							bWattSetted = 0;
							fireType = D_160w;
							bWattSetted = 1;
						}
					}else {
						bWattSetted = 0;
						fireType = D_0w;
						bMicroHeat = 0;
					}
				}
			}
		}
	}
//adjust Pro
	if (adjustTimer == 0) {
		if (bXz == 1) {
			bXz = 0;
			F_xzLedOff();
		}
		if (bSel == 1) {
			bSel = 0;
		}
		if (bYy == 1) {
			if ((bYs == 0) && (bYw == 0)) {
				PowerOff();
			}
		}
	}
}
//----------------------
void TimersProcess()
{
	if (b4ms == 1) {
		b4ms = 0;
		timer4ms++;
		timer250ms++;
		timer1s++;
	}
	//temp Pro
	if ((timer4ms >= D_4ms)) {
		timer4ms = 0;
		GetTemp();
		//triac 输出端开路测试
		if (bWattSetted) {
			zeroCounter++;
		}
	}
	//display Pro
	if (timer250ms >= D_250ms) {
		timer250ms = 0;
		DisplayProcess();
	}
	//timers pro
	if (timer1s >= D_1s) {
		//1s
		timer1s = 0;
		//user code
		if (buzzCounter > 0) {
			buzzCounter--;
			Buzz();
		}
		if (padInvalidTimer > 0) {
			padInvalidTimer--;
		}
		if (tempChangeTimer > 0) {
			tempChangeTimer--;
		}
		if (wfTimer > 0) {
			wfTimer--;
		}
		if (adjustTimer > 0) {
			adjustTimer--;
		}
		if (idleTimer > 0) {
			idleTimer--;
		}
		//
		timer1m++;
		if (timer1m >= D_1m) {
			//1m
			timer1m = 0;
			//user code
			if (yyTimer > 0) {
				yyTimer--;
			}
			if (bYy == 0) {
				if (noOperationTimer > 0) {
					noOperationTimer--;
				}
				if (workTimer > 0) {
					workTimer--;
				}
			}
			//
			timer1h++;
			if (timer1h >= D_1h) {
				timer1h = 0;
				//user code
			}
		}
	}
}
//----------------------
void UserSettingProcess()
{
	//key Process
	GetKey();
	if (bPowerOn == 1) {
		switch (keyValue) {
			case D_keyYy:
				if ((bYs == 0) && (bYw == 0)) {
					Buzz();
					bPowerOn = 1;
					bYy = 1;
					timer1h = 0;
					yyTimer += 60;
					if (yyTimer > 1440) {
						yyTimer = 60;
					}
					adjustTimer = D_xzTime;
					F_yyLedOn();
				}
				break;
			case D_keySel:
				Buzz();
				bHeat = 0;
				bMicroHeat = 0;
				bWater100 = 0;
				bSettedTime = 0;
				bWf = 0;
				bYwHw = 0;
				bYsHw = 0;
				F_xzLedOff();
				F_ysIconOff();
				F_ywIconOff();
				F_selLedOn();
				adjustTimer = D_xzTime;
				bSel = 1;
				if (bYw == 1) {
					bYw = 0;
					bYs = 1;
					bSs = 1;
					wfTimer = 1;
					settedTime = D_ysTime;
					workTimer = D_ysTime;
					adjustIndex = D_ysIndex;
					F_ysIconOn();
				}else {
					bYs = 0;
					bYw = 1;
					bSs = 1;
					wfTimer = 1;
					settedTime = D_ywTime;
					workTimer = D_ywTime;
					adjustIndex = D_ywIndex;
					F_ywIconOn();
				}
				break;
			case D_keyXz:
				if ((bYw == 1) || (bYs == 1)) {
					Buzz();
					bHeat = 0;
					bMicroHeat = 0;
					bWater100 = 0;
					bWf = 0;
					bSettedTime = 0;
					bYwHw = 0;
					bYsHw = 0;
				}
				if (bYw == 1) {
					bXz = 1;
					F_xzLedOn();
					adjustTimer = D_xzTime;
					adjustIndex++;
					if (adjustIndex == D_tableLength) {
						adjustIndex = 0;
					}
					settedTime = ywTimeTable[adjustIndex];
					workTimer = D_ywTime;
				}
				if (bYs == 1) {
					bXz = 1;
					F_xzLedOn();
					adjustTimer = D_xzTime;
					adjustIndex++;
					if (adjustIndex == D_tableLength) {
						adjustIndex = 0;
					}
					settedTime = ysTimeTable[adjustIndex];
					workTimer = D_ysTime;
				}
				break;
		}// end switch
	}
	if (keyValue == D_keyOn) {
		Buzz();
		if (bPowerOn == 0) {
			bPowerOn = 1;
			F_powerLedOn();
			bIdle = 0;
			idleTimer = D_idleTime;
		}else {
			bPowerOn = 0;
			PowerOff();
		}
	}
	keyValue = 0;
}
//----------------------
void DisplayProcess()
{
	if (bSelfTest == 1) {
		F_allSzOn();
		F_allLedOn();
		selfTestCounter++;
		if (selfTestCounter > D_selfTestCount) {
			bSelfTest = 0;
		}
		return;
	}
	if (bAlarm == 1) {
		F_displayAlarmOn();
		return;
	}
	//
	bSecFlash = ~bSecFlash;
	if (bPowerOn == 0) {
		F_displayPowerOff();
		F_kgIconOn();
		return;
	}else{
		F_displayPowerOn();
	}
	F_kgIconOff();
	if (bKettleValid == 0) {
		F_displayAdOff();
		F_jsIconOff();
		return;
	}
	if (bYy == 1) {
		F_yyLedOn();
		F_yyIconOn();
		if ((bYw == 0) && (bYs == 0)) {
			if (bSecFlash == 1) {
				F_yyLedOff();
				F_yyIconOff();
			}
			F_displayScanOff();
			DisplayYyTime(yyTimer);
			F_displayScanOn();
			return;
		}
		if (adjustTimer == 0) {
			F_displayScanOff();
			DisplayYyTime(yyTimer);
			F_displayScanOn();
			return;
		}
	}
	F_yyIconOff();
	if (bYw == 1) {
		F_ywIconOn();
		if (bSel == 1) {
			if (bSecFlash == 1) {
				F_ywIconOff();
			}
		}
		if (bXz == 1) {
			F_displayScanOff();
			DisplayNumber(settedTime);
			F_displayScanOn();
			F_jsIconOn();
			F_xzLedOn();
			if (bSecFlash == 1) {
				F_allSzOff();
				F_xzLedOff();
			}
		}else {
			if (adjustTimer > 0) {
				F_displayScanOff();
				DisplayNumber(workTimer);
				F_displayScanOn();
				F_jsIconOn();
			}else{
				if (bWater100 == 1) {
					F_displayScanOff();
					DisplayNumber(workTimer);
					F_displayScanOn();
					F_jsIconOn();
				}else {
					F_displayScanOff();
					DisplayNumber(curTemp);
					F_displayScanOn();
					F_jsIconOff();
				}
			}
		}
		return;
	}
	if (bYs == 1) {
		F_ysIconOn();
		if (bSel == 1) {
			if (bSecFlash == 1) {
				F_ysIconOff();
			}
		}
		if (bXz == 1) {
			F_displayScanOff();
			DisplayNumber(settedTime);
			F_displayScanOn();
			F_jsIconOn();
			F_xzLedOn();
			if (bSecFlash == 1) {
				F_allSzOff();
				F_xzLedOff();
			}
		}else{
			if (adjustTimer > 0) {
				F_displayScanOff();
				DisplayNumber(workTimer);
				F_displayScanOn();
				F_jsIconOn();
			}else{
				if (bWater100 == 1) {
					F_displayScanOff();
					DisplayNumber(workTimer);
					F_displayScanOn();
					F_jsIconOn();
				}else {
					F_displayScanOff();
					DisplayNumber(curTemp);
					F_displayScanOn();
					F_jsIconOff();
				}
			}
		}
		return;
	}
	if ((bYsHw == 1) || (bYwHw == 1)) {
		F_displayBw();
		F_jsIconOff();
		return;
	}
	if (bIdle == 0) {
		bIdle = 1;
		idleTimer = D_idleTime;
	}
	if (bIdle == 1) {
		if (idleTimer == 0) {
			PowerOff();
		}
	}
	return;
}
