#ifndef __buzz_h
    #define __buzz_h
//HAL--------------------
    #define P_buzz	P3_2
    #define F_buzz()        P_buzz = ~P_buzz

//Const--------------------
    #define D_buzzLastTime  25    //25*4ms(T0) = last 100ms per Buzz
//Var----------------------
    #ifdef __buzz_c
        uint8_t buzzLastTimer ;
        uint8_t buzzCounter ;
		bit bBuzz ;
    #else
        extern uint8_t buzzLastTimer ;
        extern uint8_t buzzCounter ;
		extern bit bBuzz ;
    #endif

//Function----------------------
	void Buzz(void) ;
#endif
