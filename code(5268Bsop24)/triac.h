#ifndef __triac_h
#define __triac_h

//Hal------------------
#define	P_triac	P3_3
#define F_triacOn()   P_triac = 0
#define F_triacOff()  P_triac = 1
#define P_zero   P0_0
#define F_setZero()	P_zero = 1
#define F_clrZero()	P_zero = 0
//Const-----------------
#define D_triacHoldTime   4 //base 0.25ms

//1/24
#define D_0w	0x00000000
#define D_40w   0x00000001
#define D_80w	0x00010200
#define D_120w	0x00010201
#define D_160w	0x00010301
#define D_280w	0x00130303
#define D_320w	0x00131303
#define D_360w	0x00131313
#define D_580w	0x001F0F1F
#define D_620w	0x001F1F1F
#define D_1000w	0x00ffffff

//Var-----------------
	#ifdef __triac_c
		bit bLastZero ;
		bit bZero ;
		bit bDelay ;
		bit bWattSetted ;
		bit	bAlarm ;
		uint32_t fireType ;
		uint8_t triacTimer, delayTimer ;
        uint8_t zeroCounter;
	#else
		extern bit bLastZero ;
		extern bit bZero ;
		extern bit bDelay ;
		extern bit bWattSetted ;
		extern bit	bAlarm ;
		extern uint32_t fireType ;
		extern uint8_t triacTimer, delayTimer   ;
        extern uint8_t zeroCounter;
	#endif
//Function-------------------
void InitTriac() ;


#endif
