
#ifndef __display_h
#define __display_h
//HAL------------------------------
#define F_setA()   P2_1 = 1
#define F_setB()   P0_2 = 1
#define F_setC()   P0_3 = 1
#define F_setD()   P0_1 = 1
#define F_setE()   P2_0 = 1
#define F_setF()   P1_7 = 1
#define F_setG()   P3_6 = 1

#define F_ledComOn() P1_1 = 0
#define F_com4On() P1_2 = 0
#define F_com3On() P1_6 = 0
#define F_com2On() P1_4 = 0
#define F_com1On() P1_3 = 0

//set p11 p12 p13 p14 p16 (to 1)
#define F_comsOff() P1 |= 0x5e
//clr p01 p02 p03 P17 p20 p21 p36(to 0)
#define F_segsOff() P0 &= 0xf1 ; P1_7 = 0; P2 &= 0xfc; P3_6 = 0

#define F_displayScanOff()  bDisplayScan = 0;
#define F_displayScanOn()  bDisplayScan = 1;

//Const---------------------------
//时间选择-xz  预约-yy  功能选择-sel  电源-power
//指示灯
#define D_setXzLed  0x01
#define D_clrXzLed  0xfe
#define D_setYyLed  0x10
#define D_clrYyLed  0xef
#define D_setSelLed  0x20
#define D_clrSelLed  0xdf
#define D_setPowerLed  0x40
#define D_clrPowerLed  0xbf
//数码管图标：倒计时 js；燕窝 yw；药膳 ys；预约 yy；开关 kg；
#define D_setJsFont 0x01
#define D_clrJsFont 0xfe
#define D_setYwFont	0x02
#define D_clrYwFont	0xfd
#define D_setYsFont	0x04
#define D_clrYsFont 0xfb
#define D_setYyFont	0x08
#define D_clrYyFont	0xf7
#define	D_setKgFont	0x10
#define D_clrKgFont	0xef
//数码管数字字符
#define D_fontBar  0x40
#define D_fontBlank 0x00
#define D_fontE 0x79
#define D_fontF 0x71
#define D_fontN 0x37
#define D_fontH 0x76
#define D_fontB 0x7c
#define D_font0 0x3f
#define D_font1 0x06
#define D_font3 0x4F
#define D_font8 0xff

#define D_DisplayBufferSize 5
//Var------------------------------
#ifdef __dispaly_c
    bit bDisplayScan ;
//displayBuffer[0]: Led indicators
//displayBuffer[1-4]: SZ[1-4]
    uint8_t displayBuffer[D_DisplayBufferSize] ;
#else
    extern uint8_t displayBuffer[D_DisplayBufferSize] ;
    extern bit bDisplayScan ;
#endif
//Function----------------------------
void DisplayScan(void) ;//
void DisplayNumber(uint8_t number) ;
void DisplayYyTime(uint16_t time);
//
#define F_displayAdShort()   displayBuffer[3] = D_fontE ; \
                            displayBuffer[2] = D_font1     ;  displayBuffer[1] = D_fontBlank
#define F_displayAdOff()     displayBuffer[3] = D_fontBlank ; \
                            displayBuffer[2] = D_fontE ;  displayBuffer[1] = D_font0
#define F_displayAlarmOn()     displayBuffer[4] = D_fontBlank; displayBuffer[3] = D_fontBlank ; \
                             displayBuffer[2] = D_fontE ;  displayBuffer[1] = D_font3; displayBuffer[0] = D_fontBlank
#define F_displayPowerOff()   displayBuffer[4] = D_fontBlank; displayBuffer[3] = D_font0 ; \
                            displayBuffer[2] = D_fontF; displayBuffer[1] = D_fontF;  displayBuffer[0] = D_fontBlank;
#define F_displayPowerOn()   displayBuffer[4] = D_fontBlank; displayBuffer[3] = D_font0 ; \
                                displayBuffer[2] = D_fontN; displayBuffer[1] = D_fontBlank;  displayBuffer[0] = D_fontBlank;
#define F_allSzOn()     displayBuffer[4] = D_font8 ; displayBuffer[3] = D_font8 ;\
                           displayBuffer[2] = D_font8 ; displayBuffer[1] = D_font8
#define F_allSzOff()  displayBuffer[3] = D_fontBlank ; displayBuffer[2] = D_fontBlank ;\
						   displayBuffer[1] = D_fontBlank
#define F_displayBw() displayBuffer[3] = D_fontBlank ; displayBuffer[2] = D_fontB  ;\
						   displayBuffer[1] = D_fontBlank
#define F_allLedOn()	displayBuffer[0] = D_font8
#define F_allLedOff()	displayBuffer[0] = D_fontBlank ;
#define F_xzLedOn()		displayBuffer[0] |= D_setXzLed
#define F_xzLedOff()	displayBuffer[0] &= D_clrXzLed
#define F_yyLedOn()		displayBuffer[0] |= D_setYyLed
#define F_yyLedOff()	displayBuffer[0] &= D_clrYyLed
#define F_selLedOn()	displayBuffer[0] |= D_setSelLed
#define F_selLedOff()	displayBuffer[0] &= D_clrSelLed
#define F_powerLedOn() 	displayBuffer[0] |= D_setPowerLed
#define F_powerLedOff()	displayBuffer[0] &= D_clrPowerLed


#define F_jsIconOn()		displayBuffer[4] |= D_setJsFont
#define F_jsIconOff()	    displayBuffer[4] &= D_clrJsFont
#define F_ywIconOn()		displayBuffer[4] |= D_setYwFont
#define F_ywIconOff()		displayBuffer[4] &= D_clrYwFont
#define F_ysIconOn()		displayBuffer[4] |= D_setYsFont
#define F_ysIconOff()		displayBuffer[4] &= D_clrYsFont
#define F_yyIconOn()		displayBuffer[4] |= D_setYyFont
#define F_yyIconOff()		displayBuffer[4] &= D_clrYyFont
#define F_kgIconOn()		displayBuffer[4] |= D_setKgFont
#define F_kgIconOff()		displayBuffer[4] &= D_clrKgFont

#endif
