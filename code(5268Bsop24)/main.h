
#ifndef __main_h
#define __main_h

//Hal-----------------

//Const---------------
 //basetime 4ms
#define D_4ms 1
#define D_250ms  70
#define D_1s   250
//basetime 1s
#define D_1m   60
//basetime 1m
#define D_1h   60
#define D_wfTime 255  //s
#define D_noOperationTime 20 //min
#define D_doubleClickTime  3  //3s
#define D_xzTime 5 //halfS
#define D_ywTime	30 //min
#define D_ywIndex   1
#define D_ysTime	40	//min
#define D_ysIndex   4
#define D_hwTime	60 //min
#define D_selfTestCount 4
#define D_idleTime 30 //s

//
#define D_temp  10
//keyValue: 时间选择-xz  预约-yy  功能选择-sel  电源-on
#define D_keyNull	0
#define D_keyXz	1
#define D_keyYy	2
#define D_keySel  3
#define D_keyOn	4
//
#define D_tableLength 9

#ifdef  MAIN_C
//var------------------
    bit bSelfTest;
    bit bPowerOn ;
	bit bIdle ;
    bit b500ms      ;
    bit b4ms   ;
    bit bYw ;
	bit bYs ;
    bit bYy ;
    bit bXz ;
	bit bWater100 ;
	bit bMicroHeat ;
	bit bHeat ;
    bit bSecFlash ;
	bit b100Buzz ;
	bit bSs ;
	bit bYwHw ;
	bit bYsHw ;
	bit bWf ;
	bit bSettedTime ;
    bit bSel ;
    uint8_t timer1Click, timer4ms, timer250ms, timer1s, timer1m , timer1h;
    uint8_t adjustTimer,  noOperationTimer, selfTestCounter;
	uint8_t hwTimer, workTimer, wfTimer, settedTime , idleTimer;
    uint16_t yyTimer;
    uint8_t settedTemp, hwTemp ;
    uint8_t keyValue ;
	uint8_t    adjustIndex ;
	code const  uint8_t ywTimeTable[D_tableLength] = {25,30,35,40,60,90,120,150,180} ;
	code const  uint8_t ysTimeTable[D_tableLength] = {10,15,20,30,40,50,60,90,120} ;
	code const	uint8_t ywWattTable[D_tableLength] = {4,4,3,2,1,0,0,0,0} ;
	code const 	uint8_t ysWattTable[D_tableLength] = {2,2,2,2,2,1,1,0,0} ;
	code const	uint32_t wattTable[6] = {D_120w, D_160w, D_280w,D_320w,D_360w,D_580w} ;
#else
    extern bit bPowerOn ;
    extern bit b500ms   ;
    extern bit b4ms   ;
    extern bit bYw ;
    extern bit bYs ;
    extern bit bXz ;
    extern uint8_t  timer1Click, timer4ms, timer250ms, timer1s, timer1m ;
    extern uint8_t  adjustTimer, wfTimer , noOperationTimer, doubleClickTimer;
	extern uint16_t hwTimer ;
    extern uint8_t settedTemp ;
    extern uint8_t keyValue ;

#endif
//function------------------------
    void GetKey(void);
	void PowerOff(void);
	void InitVar();
    void DisplayProcess() ;
    void TasksProcess() ;
    void TimersProcess();
    void UserSettingProcess();

#endif
