

#include	"REGtenxTM52F5268B.h"
#include    "typeAlias.h"
#include	"intrins.h"
#include 	"math.h"

#include	"global.h"
#include	"setting.h"
#include	"tkcalculate.h"
#include	"updatebaseline.h"
#include	"tkjudgement.h"
#include	"triac.h"
#include    "display.h"
#include    "buzz.h"
#include	"tempAd.h"
#include    "main.h"
